<?php

namespace App\Traits;

use Storage;
use Exception;
use ZipArchive;
use App\Custom\zipfileDIAN;
use App\Company;
use App\Mail\InvoiceMail;
use DOMDocument;
use App\TypeOperation;
use App\TypeRegime;
use App\Tax;
use App\Resolution;
use App\TypeDocument;
use App\User;
use QrCode;
use Illuminate\Support\Facades\Mail;
use InvalidArgumentException;
use Stenfrank\UBL21dian\Sign;
use PDF;

/**
 * Document trait.
 */
trait DocumentTrait
{
    /**
     * PPP.
     *
     * @var string
     */
    public $ppp = '000';

    /**
     * Payment form default.
     *
     * @var array
     */
    private $paymentFormDefault = [
        'payment_form_id' => 1,
        'payment_method_id' => 10,
    ];

    /**
     * Create xml.
     *
     * @param array $data
     *
     * @return DOMDocument
     */
    protected function createXML(array $data)
    {
        try {
            $DOMDocumentXML = new DOMDocument();
            $DOMDocumentXML->preserveWhiteSpace = false;
            $DOMDocumentXML->formatOutput = true;
            $DOMDocumentXML->loadXML(view("xml.{$data['typeDocument']['code']}", $data)->render());

//            $file = fopen("C:\\DEBUG.TXT", "w");
//            fwrite($file, json_encode($data).PHP_EOL);
//            fclose($file);        

            return $DOMDocumentXML;
        } catch (InvalidArgumentException $e) {
            throw new Exception("The API does not support the type of document '{$data['typeDocument']['name']}' Error: {$e->getMessage()}");
        } catch (Exception $e) {
            throw new Exception("Error: {$e->getMessage()}");
        }
    }

    /**
     * Create pdf.
     *
     * @param array $data
     *
     * @return DOMDocument
     */
    protected function createPDF($user, $company, $customer, $typeDocument, $resolution, $date, $time, $paymentForm, $request, $cufecude, $tipodoc = "INVOICE", $withHoldingTaxTotal = NULL, $notes = NULL)
    {
//        try {
            define("DOMPDF_ENABLE_REMOTE", true);
            $filenameLogo   = storage_path("app/public/{$company->identification_number}/{$company->identification_number}{$company->dv}.jpg");
            if(file_exists($filenameLogo))
            {
                $logoBase64     = base64_encode(file_get_contents($filenameLogo));
                $imgLogo        = "data:image/png;base64, ".$logoBase64;
            }
            else
            {
                $logoBase64     = NULL;
                $imgLogo        = NULL;
            }    
            
            $totalbase = $request->legal_monetary_totals['line_extension_amount'];

            if($tipodoc == "INVOICE"){
                $qrBase64 = base64_encode(QrCode::format('png')
                ->errorCorrection('Q')
                    ->size(220)
                        ->margin(0)
                            ->generate('NumFac: '.$request->number.PHP_EOL.'FecFac: '.$date.PHP_EOL.'NitFac: '.$company->identification_number.PHP_EOL.'DocAdq: '.$customer->company->identification_number.PHP_EOL.'ValFac: '.$request->legal_monetary_totals['tax_exclusive_amount'].PHP_EOL.'ValIva: '.$request->tax_totals[0]['tax_amount'].PHP_EOL.'ValOtroIm: '.$request->legal_monetary_totals['allowance_total_amount'].PHP_EOL.'ValTotal: '.$request->legal_monetary_totals['payable_amount'].PHP_EOL.'CUFE: '.$cufecude));
                $imageQr    =  "data:image/png;base64, ".$qrBase64;

                $pdf        =  PDF::loadView("invoice", compact("user", "company", "customer", "resolution", "date", "time", "paymentForm", "request", "cufecude", "imageQr", "imgLogo", "withHoldingTaxTotal", "notes"))->setWarnings(false);
                $filename   =  storage_path("app/public/{$company->identification_number}/FES-{$resolution->next_consecutive}.pdf");
            }  
            else
              if($tipodoc == "NC"){
                $qrBase64 = base64_encode(QrCode::format('png')
                ->errorCorrection('Q')
                    ->size(220)
                        ->margin(0)
                            ->generate('NumCr: '.$request->number.PHP_EOL.'FecCr: '.$date.PHP_EOL.'NitFac: '.$company->identification_number.PHP_EOL.'DocAdq: '.$customer->company->identification_number.PHP_EOL.'ValFac: '.$request->legal_monetary_totals['tax_exclusive_amount'].PHP_EOL.'ValIva: '.$request->tax_totals[0]['tax_amount'].PHP_EOL.'ValOtroIm: '.$request->legal_monetary_totals['allowance_total_amount'].PHP_EOL.'ValTotal: '.$request->legal_monetary_totals['payable_amount'].PHP_EOL.'CUDE: '.$cufecude));
                $imageQr    =  "data:image/png;base64, ".$qrBase64;

                $pdf        =  PDF::loadView("creditnote", compact("user", "company", "customer", "resolution", "date", "time", "paymentForm", "request", "cufecude", "imageQr", "imgLogo", "withHoldingTaxTotal", "notes"))->setWarnings(false);
                $filename   =  storage_path("app/public/{$company->identification_number}/NCS-{$resolution->next_consecutive}.pdf");
            }
              else{
                $qrBase64 = base64_encode(QrCode::format('png')
                ->errorCorrection('Q')
                    ->size(220)
                        ->margin(0)
                            ->generate('NumDb: '.$request->number.PHP_EOL.'FecDb: '.$date.PHP_EOL.'NitFac: '.$company->identification_number.PHP_EOL.'DocAdq: '.$customer->company->identification_number.PHP_EOL.'ValFac: '.$request->legal_monetary_totals['tax_exclusive_amount'].PHP_EOL.'ValIva: '.$request->tax_totals[0]['tax_amount'].PHP_EOL.'ValOtroIm: '.$request->legal_monetary_totals['allowance_total_amount'].PHP_EOL.'ValTotal: '.$request->legal_monetary_totals['payable_amount'].PHP_EOL.'CUDE: '.$cufecude));
                $imageQr    =  "data:image/png;base64, ".$qrBase64;

                $pdf        =  PDF::loadView("debitnote", compact("user", "company", "customer", "resolution", "date", "time", "paymentForm", "request", "cufecude", "imageQr", "imgLogo", "withHoldingTaxTotal", "notes"))->setWarnings(false);
                $filename   =  storage_path("app/public/{$company->identification_number}/NDS-{$resolution->next_consecutive}.pdf");
            }      

            file_put_contents($filename, $pdf->output());

//           $notificacion = $this->sendEmail($filename, compact('user','customer'));

//            if($notificacion)
            return $pdf->download($filename);

//        } catch (InvalidArgumentException $e) {
//            throw new Exception("The API does not support the type of document '{$typeDocument->name}' Error: {$e->getMessage()}");
//        } catch (Exception $e) {
//            throw new Exception("Error: {$e->getMessage()}");
//        }
    }

    /**
     * Zip base64.
     *
     * @param \App\Company              $company
     * @param \App\Resolution           $resolution
     * @param \Stenfrank\UBL21dian\Sign $sign
     *
     * @return string
     */
    protected function zipBase64(Company $company, Resolution $resolution, Sign $sign, $GuardarEn = false)
    {
        $dir = preg_replace("/[\r\n|\n|\r]+/", "", "zip/{$resolution->company_id}");
        $nameXML = preg_replace("/[\r\n|\n|\r]+/", "", $this->getFileName($company, $resolution));
        $nameZip = preg_replace("/[\r\n|\n|\r]+/", "", $this->getFileName($company, $resolution, 6, '.zip'));

        $this->pathZIP = preg_replace("/[\r\n|\n|\r]+/", "", "app/zip/{$resolution->company_id}/{$nameZip}");

        Storage::put(preg_replace("/[\r\n|\n|\r]+/", "", "xml/{$resolution->company_id}/{$nameXML}"), $sign->xml);

        if (!Storage::has($dir)) {
            Storage::makeDirectory($dir);
        }

        $zip = new ZipArchive();

        $result_code = $zip->open(storage_path($this->pathZIP), ZipArchive::CREATE);
        if($result_code !== true){
            $zip = new zipfileDIAN();
            $zip->add_file(implode("", file(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$resolution->company_id}/{$nameXML}")))), preg_replace("/[\r\n|\n|\r]+/", "", $nameXML));
			Storage::put(preg_replace("/[\r\n|\n|\r]+/", "", "zip/{$resolution->company_id}/{$nameZip}"), $zip->file());
        }
        else{
            $zip->addFile(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$resolution->company_id}/{$nameXML}")), preg_replace("/[\r\n|\n|\r]+/", "", $nameXML));
            $zip->close();
        }

        if ($GuardarEn){
            copy(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$resolution->company_id}/{$nameXML}")), $GuardarEn.".xml");
            copy(preg_replace("/[\r\n|\n|\r]+/", "", storage_path($this->pathZIP)), $GuardarEn.".zip");
        }

        return $this->ZipBase64Bytes = base64_encode(file_get_contents(preg_replace("/[\r\n|\n|\r]+/", "", storage_path($this->pathZIP))));
    }

    /**
     * Zip base64 Send Document XML.
     *
     * @param \App\Company              $company
     * @param \App\Resolution           $resolution
     * @param \Stenfrank\UBL21dian\Sign $sign
     *
     * @return string
     */
    protected function zipBase64SendDocument($passwordcertificate, $identificationnumber, $tipodoc, $documentnumber, Sign $sign, $GuardarEn = false)
    {
        $dir = preg_replace("/[\r\n|\n|\r]+/", "", "zip/{$passwordcertificate}");
        $nameXML = preg_replace("/[\r\n|\n|\r]+/", "", $this->getFileNameSendDocument($identificationnumber, $tipodoc, $documentnumber));
        $nameZip = preg_replace("/[\r\n|\n|\r]+/", "", $this->getFileNameSendDocument($identificationnumber, 'ZIP', $documentnumber, '.zip'));

        $this->pathZIP = preg_replace("/[\r\n|\n|\r]+/", "", "app/zip/{$passwordcertificate}/{$nameZip}");

        Storage::put(preg_replace("/[\r\n|\n|\r]+/", "", "xml/{$passwordcertificate}/{$nameXML}"), $sign->xml);

        if (!Storage::has($dir)) {
            Storage::makeDirectory($dir);
        }

        $zip = new ZipArchive();

        $result_code = $zip->open(storage_path($this->pathZIP), ZipArchive::CREATE);
        if($result_code !== true){
            $zip = new zipfileDIAN();
            $zip->add_file(implode("", file(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$passwordcertificate}/{$nameXML}")))), preg_replace("/[\r\n|\n|\r]+/", "", $nameXML));
			Storage::put(preg_replace("/[\r\n|\n|\r]+/", "", "zip/{$passwordcertificate}/{$nameZip}"), $zip->file());
        }
        else{
            $zip->addFile(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$passwordcertificate}/{$nameXML}")), preg_replace("/[\r\n|\n|\r]+/", "", $nameXML));
            $zip->close();
        }

        if ($GuardarEn){
            copy(preg_replace("/[\r\n|\n|\r]+/", "", storage_path("app/xml/{$passwordcertificate}/{$nameXML}")), $GuardarEn.".xml");
            copy(preg_replace("/[\r\n|\n|\r]+/", "", storage_path($this->pathZIP)), $GuardarEn.".zip");
        }

        return $this->ZipBase64Bytes = base64_encode(file_get_contents(preg_replace("/[\r\n|\n|\r]+/", "", storage_path($this->pathZIP))));
    }

    /**
     * Get file name.
     *
     * @param \App\Company    $company
     * @param \App\Resolution $resolution
     *
     * @return string
     */
    protected function getFileName(Company $company, Resolution $resolution, $typeDocumentID = null, $extension = '.xml')
    {
        $date = now();
        $prefix = (is_null($typeDocumentID)) ? $resolution->type_document->prefix : TypeDocument::findOrFail($typeDocumentID)->prefix;

        $send = $company->send()->firstOrCreate([
            'year' => $date->format('y'),
            'type_document_id' => $typeDocumentID ?? $resolution->type_document_id,
        ]);

        $name = "{$prefix}{$this->stuffedString($company->identification_number)}{$this->ppp}{$date->format('y')}{$this->stuffedString($send->next_consecutive ?? 1, 8)}{$extension}";

        $send->increment('next_consecutive');

        return $name;
    }

    /**
     * Get file name Send Document.
     *
     *
     * @return string
     */
    protected function getFileNameSendDocument($identificationnumber, $tipodoc = null, $documentnumber, $extension = '.xml')
    {
        $date = now();
        if($tipodoc == 'INVOICE')
            $prefix = 'fv';
        else
            if($tipodoc == 'NC')    
                $prefix = 'nc';
            else
                if($tipodoc == 'ND')  
                    $prefix = 'nd';
                else
                    $prefix = 'z';

        $send = $documentnumber;

        $name = "{$prefix}{$this->stuffedString($identificationnumber)}{$this->ppp}{$date->format('y')}{$this->stuffedString($documentnumber ?? 1, 8)}{$extension}";

        return $name;
    }

    /**
     * Stuffed string.
     *
     * @param string $string
     * @param int    $length
     * @param int    $padString
     * @param int    $padType
     *
     * @return string
     */
    protected function stuffedString($string, $length = 10, $padString = 0, $padType = STR_PAD_LEFT)
    {
        return str_pad($string, $length, $padString, $padType);
    }

    /**
     * Get ZIP.
     *
     * @return string
     */
    protected function getZIP()
    {
        return $this->ZipBase64Bytes;
    }

    /**
     * post sendEmail.
     *
     * @return string
     */
    protected function sendEmail(string $filename, array $data){
        $company    = $data['user'];
        $customer   = $data['customer'];

        $message = Mail::to($customer->email)->send(new InvoiceMail($data, $filename));

        return $message;
    }

    protected function ActualizarTablas(){
        // User
        $user = auth()->user();

        // type regimes

        $typeregime = TypeRegime::where('id', '!=', '')->get();
        foreach($typeregime as $regime){
            switch($regime->id){
                case '1':
                    $regime->name = 'Responsable de IVA';
                    $regime->code = '48';
                    break;
                case '2':
                    $regime->name = 'No Responsable de IVA';
                    $regime->code = '49';
                    break;
            }
            $regime->save();
        }

        // type operations

        $borrar = TypeOperation::where('id', 1);
        if($borrar != NULL)
            $borrar->delete();
        $borrar = TypeOperation::where('id', 2);
        if($borrar != NULL)
            $borrar->delete();
        $borrar = TypeOperation::where('id', 3);
        if($borrar != NULL)
            $borrar->delete();
            
        $typeoperation = TypeOperation::where('id', '>=', 4)->where('id', '<=', 12)->get();
        foreach($typeoperation as $operation){
            switch($operation->id){
                case '4':
                    $operation->name = 'Nota Débito para facturación electrónica V1 (Decreto 2242)';
                    $operation->code = '33';
                    break;
                case '5':
                    $operation->name = 'Nota Débito sin referencia a facturas';
                    $operation->code = '32';
                    break;
                case '6':
                    $operation->name = 'Nota Débito que referencia una factura electrónica';
                    $operation->code = '30';
                    break;
                case '7':
                    $operation->name = 'Nota Crédito para facturación electrónica V1 (Decreto 2242)';
                    $operation->code = '23';
                    break;
                case '8':
                    $operation->name = 'Nota Crédito sin referencia a facturas';
                    $operation->code = '22';
                    break;
                case '9':
                    $operation->name = 'AIU';
                    $operation->code = '09';
                    break;
                case '10':
                    $operation->name = 'Estandar';
                    $operation->code = '10';
                    break;
                case '11':
                    $operation->name = 'Mandatos';
                    $operation->code = '11';
                    break;
                case '12':
                    $operation->name = 'Nota Crédito que referencia una factura electrónica';
                    $operation->code = '20';
                    break;
            }
            $operation->save();
        }

        // taxes

        $taxes = Tax::where('id', '!=', '')->get();
        foreach($taxes as $tax){
            switch($tax->id){
                case '1':
                    $tax->description = 'Impuesto sobre la Ventas';
                    break;
                case '2':
                    $tax->description = 'Impuesto al Consumo Departamental';
                    break;
                case '6':
                    $tax->name = 'ReteRenta';
                    break;
            }
            $tax->save();
        }

        // type_documents

        $type_documents = TypeDocument::where('id', '==', '3')->get();
        foreach($type_documents as $type_document){
            switch($type_document->id){
                case '3':
                    $type_document->cufe_algorithm = 'CUDE-SHA384';
                    break;
            }
            $type_document->save();
        }
    }

}
